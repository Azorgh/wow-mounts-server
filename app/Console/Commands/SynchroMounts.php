<?php

namespace App\Console\Commands;

use App\Http\Models\Mount;
use App\Http\Models\MountName;
use App\Http\Services\WowService;
use Illuminate\Console\Command;

use Config;

class SynchroMounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synchro:mounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update master list of mounts.';

    private $wow;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WowService $wow)
    {
        parent::__construct();
        $this->wow = $wow;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * There is a bug on Blizzard API
         * Sometimes, the mount master list endpoint return a empty list
         * For the update, we set the cache of BattleNet plugin to false
         */
        Config::set('battlenet-api.cache', false);
        $locales = ['en_GB', 'de_DE', 'es_ES', 'fr_FR', 'it_IT', 'pl_PL', 'pt_PT', 'ru_RU'];

        foreach($locales as $locale){
            Config::set('battlenet-api.locale', $locale);
            $this->info('Get mounts for ' . $locale);
            $masterList = $this->wow->getMountMasterList();

            if(count($masterList) > 0){
                foreach($masterList as $master_mount){
                    if(!empty($master_mount->name)){
                        $mount = Mount::firstOrNew(['creatureId' => $master_mount->creatureId, 'itemId' => $master_mount->itemId]);
                        $mount_data_array = get_object_vars($master_mount);
                        unset($mount_data_array['name']);
                        $mount->fill($mount_data_array);
                        $mount->save();

                        $mountNameLocalized = MountName::firstOrNew(['mount_id' => $mount->id, 'locale' => $locale]);
                        $mountNameLocalized->name = $master_mount->name;
                        $mountNameLocalized->save();
                    }
                }
            }else{
                $this->error('No mounts found');
            }
        }

        $this->call('cache:clear');
    }
}
