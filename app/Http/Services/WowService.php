<?php

namespace App\Http\Services;
use GuzzleHttp\Client;

/**
 * Created by PhpStorm.
 * User: Rémy
 * Date: 04/09/2018
 * Time: 14:16
 */

class WowService extends \Xklusive\BattlenetApi\Services\WowService
{
    public function setApiUrl()
    {
        $this->client = new Client([
            'base_uri' => config('battlenet-api.domain')
        ]);
    }

    public function getOAuthProfile()
    {
        return $this->cache('/user/characters', ['query' => ['access_token' => \Auth::user()->token]], __FUNCTION__);
    }

    public function getRealms($region)
    {
        $realms_file = json_decode(file_get_contents(storage_path("json/realms_". $region .".json")));
        return $realms_file->realms;
    }

    public function getCharacterMounts($realm, $character_name, $options = [])
    {
        return $this->cache('/character/' .$realm . '/' . $character_name, ['query' => ['fields' => 'mounts']], __FUNCTION__);
    }

    /**
     * Get mount master list.
     *
     * A list of all supported mounts
     *
     * @param array $options Options
     *
     * @return Illuminate\Support\Collection api response
     */
    public function getMountMasterList(array $options = [])
    {
        return $this->cache('/mount/', $options, __FUNCTION__)['mounts'];
    }
}