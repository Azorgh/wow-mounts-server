<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect on Blizzard connection instead of display login form
     * @return mixed
     */
    public function showLoginForm()
    {
        return Socialite::driver('battlenet')->scopes(['wow.profile'])->redirect();
    }

    /**
     * Called by Battle.net for the return
     * Save and connect to the user (stored in our database)
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function loginCallback()
    {
        $bnet_user = Socialite::driver('battlenet')->user();
        $accessTokenResponseBody = $bnet_user->accessTokenResponseBody;

        $user = User::whereDistId($bnet_user->id)->firstOrNew(['dist_id' => $bnet_user->id]);
        $user->btag = $bnet_user->nickname;
        $user->token = $accessTokenResponseBody["access_token"];
        $user->save();

        \Auth::login($user);

        return redirect(route('app'));
    }
}