<?php

namespace App\Http\Controllers;

use App\Http\Models\Character;
use App\Http\Models\Mount;
use App\Http\Services\WowService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class WoWController extends Controller
{
    private $wow;
    public function __construct(WowService $wow)
    {
        $this->wow = $wow;
    }

    public function getCharacters()
    {
        $characters = $this->wow->getOAuthProfile()->first();

        foreach($characters as $key => $character){
            /**
             * Take only characters with lvl 10+
             */
            if($character->level >= 10){
                $user_character = Character::whereUserId(\Auth::user()->id)->whereName($character->name)->whereRealm($character->realm)
                    ->firstOrNew(['user_id' => \Auth::user()->id, 'name' => $character->name, 'realm' => $character->realm]);

                $user_character->class = $character->class;
                $user_character->race = $character->race;
                $user_character->gender = $character->gender;
                $user_character->level = $character->level;
                $user_character->thumbnail = $character->thumbnail;
                $user_character->last_modified = $character->lastModified;
                $user_character->save();
            }
        }

        return \Auth::user()->characters;
    }

    public function getRealms(Request $request)
    {
        $realms = $this->wow->getRealms($request->get('region'));
        return $realms;
    }

    public function getMounts(Request $request)
    {
        $mounts = \Cache::remember("MOUNTS_" . strtoupper($request->get('name')) . '_' . strtoupper($request->get('lang')), 120, function() use($request){
            $return = [];
            $mounts = Mount::with('namesLocalized')->get();
            \Config::set('battlenet-api.domain', "https://". strtolower($request->get('region')) .".api.battle.net");
            $this->wow->setApiUrl();

            $charsMountsItemIds = [];
            $charsMountsCreatureIds = [];

            foreach(\Auth::user()->characters as $character){
                $charMounts = $this->wow->getCharacterMounts(str_slug($character->realm), $request->get('name'))['mounts']->collected;
                $charMountsItemIds = array_column($charMounts, 'itemId');
                $charMountsCreatureId = array_column($charMounts, 'creatureId');
            }

            foreach($mounts as $key => $mount){
                if(in_array($mount->itemId, $charsMountsItemIds) && in_array($mount->creatureId, $charsMountsCreatureIds)){
                    $mount->looted = true;
                }else{
                    $mount->looted = false;
                }

                $mount->name = $mount->mountNameLocalized($request->get('lang'));

                $return[] = $mount;
            }

            return $return;
        });

        return $mounts;
    }
}
