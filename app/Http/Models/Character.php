<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $table = "user_characters";
    protected $fillable = ['user_id', 'name', 'realm'];

    /**
     * Every characters is set to an account
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
