<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MountName extends Model
{
    protected $table = 'mount_names_localized';
    protected $fillable = ['mount_id', 'locale', 'name'];
}
