<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Mount extends Model
{
    protected $fillable = ['creatureId', 'icon', 'isAquatic', 'isFlying', 'isGround', 'isJumping', 'itemId', 'qualityId', 'spellId'];
    protected $hidden = ['namesLocalized'];
    protected $appends = ['french_slug'];

    public function namesLocalized()
    {
        return $this->hasMany(MountName::class);
    }

    public function mountNameLocalized($locale)
    {
        $locales_corres = [
            'en' => 'en_GB',
            'de' => 'de_DE',
            'es' => 'es_ES',
            'fr' => 'fr_FR',
            'it' => 'it_IT',
            'pl' => 'pl_PL',
            'pt' => 'pt_PT',
            'ru' => 'ru_RU',
        ];

        if(array_key_exists($locale, $locales_corres))
            $locale = $locales_corres[$locale];
        elseif(!in_array($locale, $locales_corres))
            $locale = "en_GB"; //Set a default locale

        $name = "N/A";
        $nameLocalized = $this->namesLocalized->where('locale', $locale)->first();
        if($nameLocalized){
            $name = $nameLocalized->name;
        }



        return $name;
//        return $nameLocalized ? $nameLocalized->name : "N/A";
    }

    public function getFrenchSlugAttribute()
    {
        $frenchLocalization = $this->namesLocalized->where('locale', 'fr_FR')->first();
        $slug = null;
        if($frenchLocalization){
            $slug = str_slug($frenchLocalization->name);
        }

        return $slug;
    }
}
