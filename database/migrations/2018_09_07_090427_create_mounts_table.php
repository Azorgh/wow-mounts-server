<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creatureId');
            $table->string('icon')->nullable()->default(null);
            $table->boolean('isAquatic');
            $table->boolean('isFlying');
            $table->boolean('isGround');
            $table->boolean('isJumping');
            $table->integer('itemId');
            $table->integer('qualityId');
            $table->integer('spellId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mounts');
    }
}
