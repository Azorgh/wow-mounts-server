<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMountNamesLocalizedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mount_names_localized', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mount_id', false, true);
            $table->string('locale');
            $table->string('name');
            $table->timestamps();

            $table->foreign('mount_id')->references('id')->on('mounts')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mount_names_localized');
    }
}
