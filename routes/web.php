<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/logout', ['uses' => 'Auth\LoginController@logout']);
Route::get('/login', ['uses' => 'Auth\LoginController@showLoginForm', 'as' => 'login']);
Route::get('/login/callback', ['uses' => 'Auth\LoginController@loginCallback', 'as' => 'login.callback']);

Route::group(['middleware' => 'auth'], function(){
    Route::get('/', ['uses' => 'AppController@index', 'as' => 'app']);
});

Route::post('/character', 'WoWController@getCharacter');
Route::get('/realms', 'WoWController@getRealms');
Route::get('/mounts', 'WoWController@getMounts');
Route::get('/test','WoWController@getOAuthProfile');